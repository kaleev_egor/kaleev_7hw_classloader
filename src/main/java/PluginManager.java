
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;

public class PluginManager {

    private String pluginRootDirectory;

    public PluginManager(String pluginRootDirectory) {
        this.pluginRootDirectory = pluginRootDirectory;
    }

    public IPerson load(String pluginName, String pluginClassName) {
        File dir = new File(pluginRootDirectory);

        for (File filee : dir.listFiles()) {
            if (filee.isFile() && filee.getName().equals(pluginName)) {
                System.out.println(filee.getName());
                return loadClass(filee, pluginClassName);
            }
        }
        return null;
    }

    private static IPerson loadClass(File pluginName, String pluginClassName) {
        try {
            URL url = pluginName.toURI().toURL();

            URLClassLoader classLoader = new URLClassLoader(new URL[]{url});

            Class<?> cls = classLoader.loadClass(pluginClassName);

            IPerson person = (IPerson) cls.newInstance();

            return person;

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        }
        return null;
    }
}

