public class Main {
    public static void main(String[] args) {

        PluginManager pr = new PluginManager("pluginRootDirectory/pluginName");
        IPerson firstPerson = pr.load("lib1-1.0-SNAPSHOT.jar", "Person");
        IPerson secondPerson = pr.load("lib2-1.0-SNAPSHOT.jar", "Person");


        if(firstPerson != null)
        firstPerson.output();

        if(secondPerson != null)
        secondPerson.output();
    }
}
